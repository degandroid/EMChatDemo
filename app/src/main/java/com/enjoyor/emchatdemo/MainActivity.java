package com.enjoyor.emchatdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.et_name)
    EditText et_name;
    @Bind(R.id.et_password)
    EditText et_password;

//    String a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bt_bug)
    public void getBug() {
//        CrashReport.testJavaCrash();
//        Log.i("BUG",a);
    }

    @OnClick(R.id.bt_register)
    public void emRegister() {

        //注册失败会抛出HyphenateException
//        EMClient.getInstance().createAccount(username, pwd);//同步方法 服务器返回
        new Thread(new Runnable() {
            public void run() {
                try {
                    EMClient.getInstance().createAccount(et_name.getText().toString(), et_password.getText().toString());//同步方法 测试

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainActivity.this, "注册成功", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (final HyphenateException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            int errorCode = e.getErrorCode();
                            if (errorCode == EMError.NETWORK_ERROR) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_anomalies), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_ALREADY_EXIST) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.User_already_exists), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_AUTHENTICATION_FAILED) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.registration_failed_without_permission), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_ILLEGAL_ARGUMENT) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.illegal_user_name), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.Registration_failed), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        }).start();

    }


    @OnClick(R.id.bt_login)
    public void emLogin() {
        EMClient.getInstance().login(et_name.getText().toString(), et_password.getText().toString(), new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
//                以上两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
                Log.i("EM", "EM---MainActivity------>登录聊天服务器成功");
                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtra("name", et_name.getText().toString());
                intent.putExtra("password", et_password.getText().toString());
                startActivity(intent);
            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {
                Log.i("EM", "EM---MainActivity------>登录聊天服务器失败");
            }
        });
    }

}
